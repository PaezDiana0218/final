
//mapa//
function cargarMapaPrincipal(){
    let mapa= L.map('divMapa',{center:[4.639606010378797, -74.18822765350342], zoom:14});
    let mosaico= L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',{
        maxZoom: 18, id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

let unMarcador = L.marker([4.639606010378797, -74.188227653503427]);
unMarcador.addTo(mapa);

let poligono= L.polygon([
  [4.642899656083552,-74.19972896575928,],
  [4.63220594517197,-74.19333457946777 ],
  [ 4.630794363241483, -74.19264793395996],
  [4.636355124226238, -74.18088912963867],
  [4.638707740701256, -74.17625427246094],
  [4.646920449476433,-74.18110370635986],
  [ 4.645722768735571,-74.18346405029297],
  [4.645337799493931,-74.18646812438965],
  [ 4.647134320823181,-74.19127464294434 ],
  [4.642899656083552,-74.19972896575928, ]

])
poligono.addTo(mapa);
}

//mapa vóleibol//

var mapaDos;
function cargarMapaVoleibol(){
     mapaDos= L.map('divMapaDos',{center:[4.639606010378797, -74.18822765350342], zoom:16});
    let mosaicoDos = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',{
     maxZoom: 18,  id: 'mapbox.streets'
    });
     mosaicoDos.addTo(mapaDos);    
     let marcador= L.marker([ 4.642887625749778, -74.18854147195816
     ]);
     marcador.addTo(mapaDos);
    }


//mapa 3





function visitar() {
  let mapa = L.map('divMapauno', { center: [ 4.642887625749778, -74.18854147195816], zoom:15 });

  let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
      maxZoom: 20,
      id: 'mapbox.streets'
  });
  mosaico.addTo(mapa);

  var iglesia = L.marker([ 4.642887625749778, -74.18854147195816], {
      title: "iglesia",draggable:true,
      opacity: 1
      }).bindPopup("<h4>Iglesia</h4><li>Iglesia Pentecostal</li></a>")
      .addTo(mapa);

  
  var par1 = L.marker([   4.641498789172063, -74.19363766908646], {
      title: "Paque",draggable:true,
      opacity: 1
      }).bindPopup("<deportes</h4><li>Parque Porvenir</li></a>")
      .addTo(mapa);
      
   var Comercial = L.marker([  4.636734751505789 ,-74.18835639953613], {
        title: "Centro comercial",draggable:true,
        opacity: 1
        }).bindPopup("<comercio</h4><li>Mi Centro</li></a>")
        .addTo(mapa); 
 var metropolitano = L.marker([   4.641498789172063, -74.19363766908646], {
          title: "parque",draggable:true,
          opacity: 1
          }).bindPopup("<deportes</h4><li>Parque Porvenir</li></a>")
          .addTo(mapa); 
  var Uni = L.marker( [  4.636510183280553,-74.18544352054595], {
            title: "iglesia",draggable:true,
            opacity: 1
          }).bindPopup("<Educación</h4><li>Universidad Francisco Jose De Caldas</li></a>")
          .addTo(mapa);    
var tebolis = L.marker( [   4.6392210377997885,-74.18366253376007], {
          title: "cCentro Comercial",draggable:true,
           opacity: 1
          }).bindPopup("<Comercio</h4><li>Trebolis</li></a>")
           .addTo(mapa);            
   var peluquerias = L.marker( [   4.6392210377997885,-74.18366253376007], {
          title: "Belleza",draggable:true,
           opacity: 1
          }).bindPopup("<Belleza</h4><li>peluquerias y Barberias </li></a>")
            .addTo(mapa);  
   var peluquerias = L.marker( [ 4.63825325858382, -74.18555215001106], {
         title: "Comida",draggable:true,
           opacity: 3
           }).bindPopup("<Comidas</h4><li>Restaurantesopp   </li></a>")
           .addTo(mapa);  
                        
    }